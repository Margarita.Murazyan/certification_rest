create
    sequence hibernate_sequence start
    with 1 increment by 1;

CREATE TABLE courses
(
    id           serial,
    name         varchar(255) NOT NULL,
    teacher_name varchar(255) NOT NULL,
    description  varchar(255),
    start_date   timestamp,
    end_date     timestamp,
    created_at   timestamp    NOT NULL default CURRENT_TIMESTAMP,
    primary key (id)
);

CREATE TABLE applicants
(
    id           serial,
    name         varchar(255),
    email        varchar(255) NOT NULL,
    phone_number varchar(255),
    address      varchar(255),
    password     varchar(255) NOT NULL,
    status       int4,
    course_id    integer,
    primary key (id),
    CONSTRAINT applicants_fk1_course FOREIGN KEY (course_id) REFERENCES courses (id)

);
