package com.aca.certification_rest.model;

import com.aca.certification_rest.model.enums.ApplicantStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity
@Table(name = "applicants")
public class Applicant {


  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(columnDefinition = "serial")
  private Integer id;

  @Column
  private String  name;

  @Column(nullable = false)
  @Email(message = "Invalid email")
  private String email;

  @Column(nullable = false)
  @NotNull
  @Size(min = 8, message = "Password min length must be greater than 5")
  private String password;

  @Column
  private String phoneNumber;

  @Column
  private String address;

  @Enumerated(EnumType.ORDINAL)
  private ApplicantStatus status;

  @ManyToOne(cascade = CascadeType.REFRESH)
  private Course course;

  @PrePersist
  public void prePersist() {
    status = ApplicantStatus.ON_HOLD;
  }
}
