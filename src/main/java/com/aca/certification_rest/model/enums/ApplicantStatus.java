package com.aca.certification_rest.model.enums;

public enum ApplicantStatus {
    ON_HOLD, IN_PROGRESS, COMPLETED
}
