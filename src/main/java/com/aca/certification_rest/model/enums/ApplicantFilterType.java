package com.aca.certification_rest.model.enums;

public enum ApplicantFilterType {
    email, name, course
}
