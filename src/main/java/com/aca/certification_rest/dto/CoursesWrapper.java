package com.aca.certification_rest.dto;


import com.aca.certification_rest.model.Applicant;
import com.aca.certification_rest.model.Course;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class CoursesWrapper {

    private List<Course> courses;
    private int totalPage;
}
