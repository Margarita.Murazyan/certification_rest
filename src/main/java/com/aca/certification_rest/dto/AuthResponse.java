package com.aca.certification_rest.dto;

import com.aca.certification_rest.model.Applicant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class AuthResponse {

    private boolean success;
    private String message;
    private Applicant applicant;
    private String token;
}
