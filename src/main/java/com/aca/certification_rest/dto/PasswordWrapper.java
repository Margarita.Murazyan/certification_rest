package com.aca.certification_rest.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PasswordWrapper {
    private String oldPassword;

    @NotNull
    @Size(min = 8, message = "Password min length must be greater than 5")
    private String newPassword;
}
