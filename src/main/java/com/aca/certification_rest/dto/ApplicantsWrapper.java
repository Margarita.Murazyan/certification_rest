package com.aca.certification_rest.dto;


import com.aca.certification_rest.model.Applicant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ApplicantsWrapper {

    private List<Applicant> applicants;
    private int totalPage;
}
