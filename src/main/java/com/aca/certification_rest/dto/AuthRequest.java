package com.aca.certification_rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class AuthRequest {


    @Email(message = "Invalid email")
    private String email;

    @NotNull
    @Size(min = 8, message = "Password min length must be greater than 5")
    private String password;
}
