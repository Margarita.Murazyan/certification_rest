package com.aca.certification_rest.service;

import com.aca.certification_rest.model.Applicant;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface CertificateService {

    void generateCertificateFile(Applicant applicant);

    void getAllCertificates(HttpServletResponse httpServletResponse);

    void getApplicantCertificate(Applicant applicant, HttpServletResponse httpServletResponse);

    String createZipFile(List<String> certPaths);


}
