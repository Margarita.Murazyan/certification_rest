package com.aca.certification_rest.service;

import com.aca.certification_rest.dto.ApplicantsWrapper;
import com.aca.certification_rest.dto.AuthRequest;
import com.aca.certification_rest.dto.AuthResponse;
import com.aca.certification_rest.dto.StateInformationResponse;
import com.aca.certification_rest.model.Applicant;
import com.aca.certification_rest.model.enums.ApplicantFilterType;
import org.springframework.http.ResponseEntity;

public interface ApplicantService {

    AuthResponse saveNewApplicant(Applicant applicant);

    ResponseEntity login(AuthRequest authRequest);

    AuthResponse changePassword(Applicant applicant, String oldPassword, String newPassword);

    ApplicantsWrapper getApplicantsBy(ApplicantFilterType filterType, int page, int perPage, int courseId, String text);

    StateInformationResponse assignCourse(Applicant applicant, Integer course);

    StateInformationResponse unAssignCourse(Applicant applicant);

    StateInformationResponse changeCourseStatus(Applicant applicant, String status);
}
