package com.aca.certification_rest.service.impl;

import com.aca.certification_rest.dto.ApplicantsWrapper;
import com.aca.certification_rest.dto.AuthRequest;
import com.aca.certification_rest.dto.AuthResponse;
import com.aca.certification_rest.dto.StateInformationResponse;
import com.aca.certification_rest.model.Applicant;
import com.aca.certification_rest.model.Course;
import com.aca.certification_rest.model.enums.ApplicantFilterType;
import com.aca.certification_rest.model.enums.ApplicantStatus;
import com.aca.certification_rest.repository.ApplicantRepository;
import com.aca.certification_rest.repository.CourseRepository;
import com.aca.certification_rest.security.JwtTokenUtil;
import com.aca.certification_rest.service.ApplicantService;
import com.aca.certification_rest.service.CertificateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ApplicantServiceImpl implements ApplicantService {

    private ApplicantRepository applicantRepository;
    private CourseRepository courseRepository;
    private PasswordEncoder passwordEncoder;
    private JwtTokenUtil jwtTokenUtil;
    private CertificateService certificateService;

    @Autowired
    private ApplicantServiceImpl(ApplicantRepository applicantRepository,CourseRepository courseRepository,
                                 PasswordEncoder passwordEncoder, JwtTokenUtil jwtTokenUtil,
                                 CertificateService certificateService) {
        this.jwtTokenUtil = jwtTokenUtil;
        this.applicantRepository = applicantRepository;
        this.courseRepository = courseRepository;
        this.passwordEncoder = passwordEncoder;
        this.certificateService = certificateService;
    }

    @Override
    public AuthResponse saveNewApplicant(Applicant applicant) {
        AuthResponse response = AuthResponse.builder()
                .message("Email already used.").build();
        if (!applicantRepository.existsByEmail(applicant.getEmail().toLowerCase())) {
            applicant.setPassword(passwordEncoder.encode(applicant.getPassword()));
            applicant.setEmail(applicant.getEmail().toLowerCase());
            response.setMessage("Success");
            response.setMessage("Success");
            response.setSuccess(true);
            applicant = applicantRepository.save(applicant);
            response.setApplicant(applicant);
            response.setToken(jwtTokenUtil.generateTokenForApplicant(applicant));
        }
        return response;
    }

    @Override
    public ResponseEntity login(AuthRequest authRequest) {
        Applicant admin = applicantRepository.findByEmail(authRequest.getEmail().toLowerCase());
        HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
        AuthResponse body = AuthResponse.builder()
                .message("Bad credentials!")
                .build();
        if (admin != null && passwordEncoder.matches(authRequest.getPassword(), admin.getPassword())) {
            httpStatus = HttpStatus.OK;
            body = AuthResponse.builder()
                    .message("Success")
                    .success(true)
                    .applicant(admin)
                    .token(jwtTokenUtil.generateTokenForApplicant(admin))
                    .build();
        }
        return ResponseEntity.status(httpStatus).body(body);
    }

    @Override
    public AuthResponse changePassword(Applicant applicant, String oldPassword, String newPassword) {
        AuthResponse response = AuthResponse.builder()
                .message("Bad credentials!")
                .build();
        if(passwordEncoder.matches( oldPassword, applicant.getPassword())){
            applicant.setPassword(passwordEncoder.encode(newPassword));
            applicant = applicantRepository.save(applicant);
            String token = jwtTokenUtil.generateTokenForApplicant(applicant);
            response = AuthResponse.builder()
                    .message("Success")
                    .token(token)
                    .success(true)
                    .applicant(applicant)
                    .build();
        }
        return response;
    }

    @Override
    public ApplicantsWrapper getApplicantsBy(ApplicantFilterType filterType, int page, int perPage, int courseId, String text) {
        ApplicantsWrapper response = new ApplicantsWrapper() ;
        Page<Applicant> data = null;
        switch (filterType){
            case name:
                data = applicantRepository.findAllByNameLike(text, PageRequest.of(page, perPage));
                break;
            case email:
                data = applicantRepository.findAllByEmailLike(text, PageRequest.of(page, perPage));
                break;
            case course:
                data = applicantRepository.findAllByCourse(Course.builder().id(courseId).build(),PageRequest.of(page,perPage));
                break;
        }
        response.setApplicants(data.getContent());
        response.setTotalPage(data.getTotalPages());
        return response;
    }

    @Override
    public StateInformationResponse assignCourse(Applicant applicant, Integer course) {
        StateInformationResponse response = new StateInformationResponse();
        response.setMessage("You already have a course");
        if(!courseRepository.existsById(course)){
            response.setMessage("Course not found");
            return response;
        }
        if(applicant.getCourse()==null){
            applicant.setCourse(Course.builder().id(course).build());
            applicantRepository.save(applicant);
            log.info("Assigned course with id =" +course+" to applicant "+applicant );
            response.setMessage("Success");
            response.setSuccess(true);
        }
        return response;
    }

    @Override
    public StateInformationResponse unAssignCourse(Applicant applicant) {
        applicant.setCourse(null);
        applicantRepository.save(applicant);
        return new StateInformationResponse("Success", true);
    }

    @Override
    public StateInformationResponse changeCourseStatus(Applicant applicant, String status) {
        if(applicant.getCourse()==null){
            return new StateInformationResponse("There is no a course", false);
        }
        if(status.equals(ApplicantStatus.COMPLETED.name())){
            certificateService.generateCertificateFile(applicant);
        }
        applicant.setStatus(ApplicantStatus.valueOf(status));
        applicantRepository.save(applicant);
        return new StateInformationResponse("Success", true);

    }


}
