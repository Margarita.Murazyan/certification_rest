package com.aca.certification_rest.service.impl;

import com.aca.certification_rest.model.Applicant;
import com.aca.certification_rest.model.enums.ApplicantStatus;
import com.aca.certification_rest.repository.ApplicantRepository;
import com.aca.certification_rest.service.CertificateService;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
public class CertificateServiceImpl implements CertificateService {


    String templateName = "CertificateTemplate.doc";


    @Value("${certificates.folder}")
    private String certificatesFolderPath;

    private ApplicantRepository applicantRepository;

    @Autowired
    public CertificateServiceImpl(ApplicantRepository applicantRepository) {
        this.applicantRepository = applicantRepository;
    }


    @Override
    public void generateCertificateFile(Applicant applicant) {
        if (applicant.getStatus() == ApplicantStatus.COMPLETED) {
            Document document = new Document();
            try {
                File userFolder = new File(certificatesFolderPath + applicant.getId());
                if (!userFolder.exists()) {
                    userFolder.mkdirs();
                }
                File file = new File(certificatesFolderPath + applicant.getId() + "\\" + applicant.getCourse().getName() + "_" + applicant.getCourse().getId() + ".pdf");
                if (!file.exists()) {
                    file.createNewFile();
                    PdfWriter.getInstance(document, new FileOutputStream(certificatesFolderPath + applicant.getId() + "\\" + applicant.getCourse().getName() + "_" + applicant.getCourse().getId() + ".pdf"));

                    convertFormTemplateToPdf(document, applicant);
                }
            } catch (DocumentException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }


    public String generateCertificateFile(Applicant applicant, String path) {
        String generatedCertPath = "";
        if (applicant.getStatus() == ApplicantStatus.COMPLETED) {
            Document document = new Document();
            try {
                File userFolder = new File(path);
                if (!userFolder.exists()) {
                    userFolder.mkdirs();
                }
                File file = new File(path + "\\" + applicant.getCourse().getName() + "_" + applicant.getId() + ".pdf");
                if (!file.exists()) {
                    file.createNewFile();
                    PdfWriter.getInstance(document, new FileOutputStream(path + "\\" + applicant.getCourse().getName() + "_" + applicant.getId() + ".pdf"));
                    generatedCertPath = path  + "\\" + applicant.getCourse().getName() + "_" + applicant.getId() + ".pdf";
                    convertFormTemplateToPdf(document, applicant);
                }
            } catch (DocumentException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return generatedCertPath;
    }

    @Override
    public void getAllCertificates(HttpServletResponse httpServletResponse) {
        List<Applicant> completedApplicants = applicantRepository.findAllByStatusAndCourseIsNotNull(ApplicantStatus.COMPLETED);
        List<String> certPaths = new ArrayList<>();
        for (Applicant completedApplicant : completedApplicants) {
            certPaths.add(generateCertificateFile(completedApplicant, certificatesFolderPath + "temp\\"));
        }
        if (completedApplicants.size() > 0) {
            String zipFilePath = createZipFile(certPaths);
            FileInputStream is = null;
            try {
                is = new FileInputStream(zipFilePath);
                IOUtils.copy(is, httpServletResponse.getOutputStream());

                File tempFolder = new File(certificatesFolderPath + "temp");// delete temp folder
                Arrays.stream(tempFolder.listFiles()).forEach(f -> f.delete());
                tempFolder.delete();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void getApplicantCertificate(Applicant applicant, HttpServletResponse httpServletResponse) {
        if (applicant.getCourse() != null && applicant.getStatus() == ApplicantStatus.COMPLETED) {
            generateCertificateFile(applicant);
            try {
                File file = new File(certificatesFolderPath + applicant.getId() + "\\" + applicant.getCourse().getName() + "_" + applicant.getCourse().getId() + ".pdf");
                FileInputStream is = new FileInputStream(file);
                IOUtils.copy(is, httpServletResponse.getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public String readFormTemplate() {
        String text = "";
        WordExtractor extractor = null;
        try {
            Resource resource = new ClassPathResource(templateName);

            InputStream fis = resource.getInputStream();
            HWPFDocument document = new HWPFDocument(fis);
            extractor = new WordExtractor(document);
            String[] fileData = extractor.getParagraphText();
            for (int i = 0; i < fileData.length; i++) {
                if (fileData[i] != null)
                    text += fileData[i];
            }
        } catch (Exception exep) {
            exep.printStackTrace();
        }
        return text;
    }

    public void convertFormTemplateToPdf(Document documentPDF, Applicant applicant) {
        Font font = FontFactory.getFont(FontFactory.COURIER, 14, BaseColor.BLACK);
        WordExtractor extractor = null;
        try {
            Resource resource = new ClassPathResource(templateName);
            InputStream fis = resource.getInputStream();
            HWPFDocument document = new HWPFDocument(fis);
            extractor = new WordExtractor(document);
            String[] fileData = extractor.getParagraphText();
            documentPDF.open();
            for (int i = 0; i < fileData.length; i++) {
                if (fileData[i] != null) {
                    String row = fileData[i];
                    row = row.replace("<COURSE_NAME>", applicant.getCourse().getName());
                    row = row.replace("<APPLICANT_NAME>", applicant.getName());
                    row = row.replace("<TEACHER_NAME>", applicant.getCourse().getTeacherName());
                    long diff = ChronoUnit.MONTHS.between(applicant.getCourse().getEndDate().toLocalDateTime(), applicant.getCourse().getStartDate().toLocalDateTime());
                    row = row.replace("<COURSE_DURATION>", Math.abs(diff) + " month");
                    Chunk chunk = new Chunk(row, font);
                    documentPDF.add(chunk);
                    documentPDF.add(new Paragraph(""));
                }
            }
            documentPDF.close();
        } catch (Exception exep) {
            exep.printStackTrace();
        }
    }

    public String createZipFile(List<String> certPaths) {
        String zipFile = certificatesFolderPath + "certificates_" + System.currentTimeMillis() + ".zip";

        try {

            // create byte buffer
            byte[] buffer = new byte[1024];

            FileOutputStream fos = new FileOutputStream(zipFile);

            ZipOutputStream zos = new ZipOutputStream(fos);

            for (int i = 0; i < certPaths.size(); i++) {

                File srcFile = new File(certPaths.get(i));

                FileInputStream fis = new FileInputStream(srcFile);

                // begin writing a new ZIP entry, positions the stream to the start of the entry data
                zos.putNextEntry(new ZipEntry(srcFile.getName()));

                int length;

                while ((length = fis.read(buffer)) > 0) {
                    zos.write(buffer, 0, length);
                }

                zos.closeEntry();

                // close the InputStream
                fis.close();

            }

            // close the ZipOutputStream
            zos.close();

        } catch (IOException ioe) {
            System.out.println("Error creating zip file: " + ioe);
        }
        return zipFile;
    }

}
