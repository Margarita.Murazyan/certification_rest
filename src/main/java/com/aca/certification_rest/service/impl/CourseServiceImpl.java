package com.aca.certification_rest.service.impl;

import com.aca.certification_rest.dto.CoursesWrapper;
import com.aca.certification_rest.model.Course;
import com.aca.certification_rest.repository.CourseRepository;
import com.aca.certification_rest.service.CourseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class CourseServiceImpl implements CourseService {

    private CourseRepository courseRepository;


    @Autowired
    public CourseServiceImpl(CourseRepository courseRepository){
        this.courseRepository = courseRepository;
    }

    @Override
    public Course save(Course course) {
        return courseRepository.save(course);
    }

    @Override
    public CoursesWrapper searchCourseByName(String nameText, int page, int perPage) {
        Page<Course> data = courseRepository.findAllByNameLike(nameText, PageRequest.of(page, perPage));
        return CoursesWrapper.builder()
                .courses(data.getContent())
                .totalPage(data.getTotalPages())
                .build();
    }
}
