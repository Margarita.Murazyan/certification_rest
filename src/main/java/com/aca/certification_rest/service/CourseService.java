package com.aca.certification_rest.service;

import com.aca.certification_rest.dto.CoursesWrapper;
import com.aca.certification_rest.model.Course;

public interface CourseService {
    Course save(Course course);

    CoursesWrapper searchCourseByName(String nameText, int page, int perPage);
}
