package com.aca.certification_rest.repository;

import com.aca.certification_rest.model.Applicant;
import com.aca.certification_rest.model.Course;
import com.aca.certification_rest.model.enums.ApplicantStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ApplicantRepository extends JpaRepository<Applicant,Integer> {

    Applicant findByEmail(String email);

    boolean existsByEmail(String email);

    List<Applicant> findAllByStatusAndCourseIsNotNull(ApplicantStatus status);

    @Query(value = "select a from Applicant a where lower(a.name) like concat('%',lower(:key),'%')")
    Page<Applicant> findAllByNameLike(@Param("key") String nameTxt, Pageable pageable);

    @Query(value = "select a from Applicant a where lower(a.email) like concat('%',lower(:key),'%')")
    Page<Applicant> findAllByEmailLike(@Param("key") String nameTxt, Pageable pageable);

    Page<Applicant> findAllByCourse(Course course, Pageable pageable);
}
