package com.aca.certification_rest.repository;

import com.aca.certification_rest.model.Course;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends JpaRepository<Course, Integer> {

    @Query(value = "select c from Course c where lower(c.name) like concat('%',lower(:key),'%')")
    Page<Course> findAllByNameLike(@Param("key") String nameTxt, Pageable pageable);
}
