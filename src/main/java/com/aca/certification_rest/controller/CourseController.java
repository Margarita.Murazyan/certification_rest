package com.aca.certification_rest.controller;


import com.aca.certification_rest.dto.CoursesWrapper;
import com.aca.certification_rest.model.Course;
import com.aca.certification_rest.service.CourseService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/course")
public class CourseController {

    private CourseService courseService;


    @Autowired
    public CourseController(CourseService courseService){
        this.courseService = courseService;
    }



    /**
     * Add new course
     *
     * @param course           <description> new created course </description>
     * @return httpStatus 200, body {@link Course}
     * @since 20.08.2021
     */
    @PostMapping("/add")
    @ApiOperation("Add new course")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "CREATED", response = Course.class),
    })
    public ResponseEntity addNewCourse(@RequestBody @Valid Course course){
        return ResponseEntity.ok(courseService.save(course));
    }


    /**
     * Get course by named
     *
     * @param nameText           <description>course name </description>
     * @return httpStatus 200, body {@link com.aca.certification_rest.dto.CoursesWrapper}
     * @since 20.08.2021
     */
    @GetMapping("/search")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = CoursesWrapper.class),
    })
    @ApiOperation("Search course by name")
    public ResponseEntity searchCoursesByName(@RequestParam(name = "text")String nameText,
                                              @RequestParam(name = "page", required = false, defaultValue = "0")int page,
                                              @RequestParam(name = "perPage", required = false, defaultValue = "15")int perPage){
        return ResponseEntity.ok(courseService.searchCourseByName(nameText,  page, perPage));
    }

}
