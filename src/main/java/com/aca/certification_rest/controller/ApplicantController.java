package com.aca.certification_rest.controller;


import com.aca.certification_rest.dto.AuthResponse;
import com.aca.certification_rest.dto.PasswordWrapper;
import com.aca.certification_rest.dto.StateInformationResponse;
import com.aca.certification_rest.model.enums.ApplicantFilterType;
import com.aca.certification_rest.security.CurrentApplicant;
import com.aca.certification_rest.service.ApplicantService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/applicant")
public class ApplicantController {

    private ApplicantService applicantService;

    @Autowired
    public ApplicantController(ApplicantService applicantService) {
        this.applicantService = applicantService;
    }

    /**
     * @since 20.08.2021
     */
    @GetMapping("/me")
    @ApiOperation("Get personal data")
    @ApiResponses(value = {
            @ApiResponse(code = 401, message = "UNAUTHORIZED"),
    })
    public ResponseEntity me(@AuthenticationPrincipal CurrentApplicant currentApplicant) {
        return ResponseEntity.ok(currentApplicant.getApplicant());
    }


    /**
     * Change applicant password
     *
     * @param currentApplicant <description> current applican</description>
     * @param passwordWrapper  <description> new and old passwords wrapper </description>
     * @return httpStatus 200 or 400, body {@link AuthResponse}
     * @since 20.08.2021
     */
    @PutMapping("/password")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = AuthResponse.class),
            @ApiResponse(code = 400, message = "BAD_REQUEST", response = AuthResponse.class),
            @ApiResponse(code = 401, message = "UNAUTHORIZED"),
    })
    @ApiOperation("Change password")
    @PreAuthorize("hasAuthority('APPLICANT')")
    public ResponseEntity changePassword(@AuthenticationPrincipal CurrentApplicant currentApplicant,
                                         @RequestBody @Valid PasswordWrapper passwordWrapper) {
        AuthResponse authResponse = applicantService.changePassword(currentApplicant.getApplicant(), passwordWrapper.getOldPassword(), passwordWrapper.getNewPassword());
        ResponseEntity responseEntity;
        if (authResponse.isSuccess()) {
            responseEntity = ResponseEntity.ok(authResponse);
        } else {
            responseEntity = ResponseEntity.badRequest().body(authResponse);
        }
        return responseEntity;
    }



    @GetMapping("/filter-by/{filterType}")
    @ApiOperation("Filter applicants")
    public ResponseEntity filterApplicants(@PathVariable("filterType") ApplicantFilterType filterType,
                                           @RequestParam(name = "page", required = false, defaultValue = "0") int page,
                                           @RequestParam(name = "perPage", required = false, defaultValue = "15") int perPage,
                                           @RequestParam(name = "text", required = false, defaultValue = "") String text,
                                           @RequestParam(name = "courseId", required = false, defaultValue = "0") int courseId) {
        return ResponseEntity.ok(applicantService.getApplicantsBy(filterType, page, perPage, courseId, text));
    }


    /**
     * Assign course to the applicant
     *
     * @param currentApplicant <description> current applicant</description>
     * @param course           <description> assigned course </description>
     * @return httpStatus 200 or 400, body {@link StateInformationResponse}
     * @since 20.08.2021
     */
    @PutMapping("/assign-course/{course}")
    @PreAuthorize("hasAuthority('APPLICANT')")
    @ApiOperation("Assign course to the applicant")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "CREATED", response = StateInformationResponse.class),
            @ApiResponse(code = 400, message = "BAD_REQUEST", response = StateInformationResponse.class),
            @ApiResponse(code = 401, message = "UNAUTHORIZED"),
    })
    public ResponseEntity assignCourse(@PathVariable("course") Integer course,
                                       @AuthenticationPrincipal CurrentApplicant currentApplicant) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        StateInformationResponse responseBody = applicantService.assignCourse(currentApplicant.getApplicant(), course);
        if (responseBody.isSuccess()) {
            status = HttpStatus.CREATED;
        }
        return ResponseEntity.status(status).body(responseBody);
    }


    /**
     * UnAssign course for applicant
     *
     * @param currentApplicant <description> current applicant</description>
     * @return httpStatus 203 or 400, body {@link StateInformationResponse}
     * @since 20.08.2021
     */
    @DeleteMapping("/course")
    @PreAuthorize("hasAuthority('APPLICANT')")
    @ApiOperation("Assign course to the applicant")
    @ApiResponses(value = {
            @ApiResponse(code = 203, message = "Ok", response = StateInformationResponse.class),
            @ApiResponse(code = 401, message = "UNAUTHORIZED"),
    })
    public ResponseEntity unAssignCourse(@AuthenticationPrincipal CurrentApplicant currentApplicant) {
        StateInformationResponse responseBody = applicantService.unAssignCourse(currentApplicant.getApplicant());
        return ResponseEntity.status(HttpStatus.NON_AUTHORITATIVE_INFORMATION).body(responseBody);
    }


    /**
     * Change course status
     *
     * @param currentApplicant <description> current applicant</description>
     * @return httpStatus 203 or 401, body {@link StateInformationResponse}
     * @since 20.08.2021
     */
    @PutMapping("/courseStatus")
    @ApiOperation("Change course status")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = StateInformationResponse.class),
            @ApiResponse(code = 400, message = "Ok", response = StateInformationResponse.class),
            @ApiResponse(code = 401, message = "UNAUTHORIZED"),
    })
    @PreAuthorize("hasAuthority('APPLICANT')")
    public ResponseEntity changeCourseStatus(@RequestParam(name = "status") String status,
                                             @AuthenticationPrincipal CurrentApplicant currentApplicant) {
        StateInformationResponse response = applicantService.changeCourseStatus(currentApplicant.getApplicant(), status);
        HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
        if(response.isSuccess()){
            httpStatus = HttpStatus.OK;
        }
        return ResponseEntity.status(httpStatus).body(response);
    }
}
