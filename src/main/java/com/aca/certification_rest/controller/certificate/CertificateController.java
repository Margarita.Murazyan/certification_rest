package com.aca.certification_rest.controller.certificate;


import com.aca.certification_rest.security.CurrentApplicant;
import com.aca.certification_rest.service.CertificateService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/certificate")
public class CertificateController {


    private CertificateService certificateService;

    @Autowired
    public CertificateController(CertificateService certificateService){
        this.certificateService = certificateService;
    }

    @GetMapping("/my")
    @PreAuthorize("hasAuthority('APPLICANT')")
    @ApiOperation("Get applicant certificate")
    public void getCertificatesZipFile(@AuthenticationPrincipal CurrentApplicant currentApplicant,
                                       HttpServletResponse httpServletResponse){
        certificateService.getApplicantCertificate(currentApplicant.getApplicant(),httpServletResponse);
    }

    @GetMapping("/all")
    @ApiOperation("Get applicant certificate")
    public void getCertificatesZipFile(HttpServletResponse httpServletResponse){
        certificateService.getAllCertificates(httpServletResponse);
    }
}
