package com.aca.certification_rest.controller.auth;


import com.aca.certification_rest.dto.AuthRequest;
import com.aca.certification_rest.dto.AuthResponse;
import com.aca.certification_rest.model.Applicant;
import com.aca.certification_rest.service.ApplicantService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
public class AuthController {

    private ApplicantService applicantService;

    @Autowired
    public AuthController(ApplicantService applicantService){
        this.applicantService = applicantService;
    }

    /**
     * applicant login api
     *
     * @param authRequest // {email, password}
     * @return code 401 or 200, body` {@link AuthResponse}
     * @created 20.08.2021
     */
    @PostMapping("/login")
    @ApiOperation(value = "Applicant login")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = AuthResponse.class),
            @ApiResponse(code = 401, message = "UNAUTHORIZED", response = AuthResponse.class),
            @ApiResponse(code = 400, message = "BAD_REQUEST"),
    })
    public ResponseEntity login(@RequestBody @Valid AuthRequest authRequest){
        return applicantService.login(authRequest);
    }


    /**
     * Register new user
     *
     * @param applicant <description> New created applicant </description>
     * @return httpStatus 200 or 400, body {@link AuthResponse}
     * @since 20.08.2021
     */
    @PostMapping("/register")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = AuthResponse.class),
            @ApiResponse(code = 400, message = "BAD_REQUEST", response = AuthResponse.class),
    })
    @ApiOperation("Register new applicant")
    public ResponseEntity register(@RequestBody @Valid Applicant applicant){
        AuthResponse authResponse = applicantService.saveNewApplicant(applicant);
        ResponseEntity responseEntity;
        if(authResponse.isSuccess()){
            responseEntity = ResponseEntity.ok(authResponse);
        }else {
            responseEntity = ResponseEntity.badRequest().body(authResponse);
        }
        return responseEntity;
    }
}
