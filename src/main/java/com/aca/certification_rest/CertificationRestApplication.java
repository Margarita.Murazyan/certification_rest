package com.aca.certification_rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CertificationRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(CertificationRestApplication.class, args);
    }

}
