package com.aca.certification_rest.security;


import com.aca.certification_rest.model.Applicant;
import com.aca.certification_rest.repository.ApplicantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

    @Autowired
    private CurrentPrincipalDetailsService currentPrincipalDetailsService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;


    @Autowired
    private ApplicantRepository applicantRepository;


    @Value("${jwt.header}")
    private String tokenHeader;


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        response.setHeader("Access-Control-Allow-Headers", "*");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "*");
        final String requestHeader = request.getHeader(this.tokenHeader);
        String currentApplicantEmail = null;
        String currentUserPhoneNumber = null;
        String authToken = null;
        Applicant currentApplicant = null;
        if (requestHeader != null && requestHeader.startsWith("Bearer ")) {
            authToken = requestHeader.substring(7);
            try {
                jwtTokenUtil.getSubjectFromToken(authToken);
            } catch (Exception e) {
                response.sendError(401, "Unauthorized");
                return;
            }
            int id = (int) jwtTokenUtil.getAllClaimsFromToken(authToken).get("id");
            currentApplicant = applicantRepository.findById(id).orElse(null);
            if (currentApplicant == null) {//when applicant have been deleted from our system
                response.sendError(401, "You have been deleted from our system, please register again");
                return;
            } else {
                try {
                    currentApplicantEmail = jwtTokenUtil.getSubjectFromToken(authToken);
                } catch (Exception e) {
                    logger.error(e);
                }
            }
        }


        if (currentApplicant != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            int tokenId = (int) jwtTokenUtil.getAllClaimsFromToken(authToken).get("id");
            if (currentApplicant != null) { //secure applicant
                String applicantEmail = currentApplicant.getEmail();
                UserDetails userDetails = this.currentPrincipalDetailsService.loadUserByUsername(applicantEmail);
                if (currentApplicantEmail != null && jwtTokenUtil.validateToken(authToken, userDetails.getUsername()) &&
                        currentApplicant.getId() == tokenId && currentApplicant.getEmail().equals(currentApplicantEmail)) {
                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }
        }
        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Credentials", "true");
        chain.doFilter(request, response);
    }
}

