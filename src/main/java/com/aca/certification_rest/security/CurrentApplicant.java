package com.aca.certification_rest.security;


import com.aca.certification_rest.model.Applicant;
import org.springframework.security.core.authority.AuthorityUtils;

public class CurrentApplicant extends org.springframework.security.core.userdetails.User {

    private Applicant applicant;

    public CurrentApplicant(Applicant applicant) {
        super(applicant.getEmail(), applicant.getPassword(),true, true, true, true, AuthorityUtils.createAuthorityList("APPLICANT"));
        this.applicant = applicant;
    }

    public Applicant getApplicant() {
        return applicant;
    }

}
