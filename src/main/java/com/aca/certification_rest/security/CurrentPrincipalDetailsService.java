package com.aca.certification_rest.security;

import com.aca.certification_rest.model.Applicant;
import com.aca.certification_rest.repository.ApplicantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Service
public class CurrentPrincipalDetailsService implements UserDetailsService {

    private ApplicantRepository applicantRepository;

    @Autowired
    public CurrentPrincipalDetailsService(ApplicantRepository applicantRepository) {
        this.applicantRepository = applicantRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        if (validateMail(email)) {  //admin
            Applicant applicant = applicantRepository.findByEmail(email);
            if (applicant == null) {
                throw new UsernameNotFoundException("Username not found");
            }
            return new CurrentApplicant(applicant);
        } else {
            throw new UsernameNotFoundException("Username not found");
        }
    }


    private final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    private boolean validateMail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }
}
